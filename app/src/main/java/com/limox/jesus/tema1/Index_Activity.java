package com.limox.jesus.tema1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Interpolator;
import android.content.Intent;

public class Index_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
    }

    public void getOnClick(View view) {

        Intent intent;
        switch (view.getId()) {

            case R.id.btnExercise1:
                // Create the intent
                intent = new Intent(Index_Activity.this, CoinConverter_Activity.class);
                // Start the Activity
                startActivity(intent);
                break;

            case R.id.btnExercise2:
                // Create the intent
                intent = new Intent(Index_Activity.this, CmToInches_Activity.class);
                // Start the Activity
                startActivity(intent);
                break;

            case R.id.btnExercise3:
                // Create the intent
                intent = new Intent(Index_Activity.this, Web_Searcher_Activity.class);
                // Start the Activity
                startActivity(intent);
                break;

            case R.id.btnExercise4:
                // Create the intent
                intent = new Intent(Index_Activity.this, Countdown_Activity.class);
                // Start the Activity
                startActivity(intent);
                break;

            case R.id.btnExercise5:
                // Create the intent
                intent = new Intent(Index_Activity.this, Hangman_Activity.class);
                // Start the Activity
                startActivity(intent);
                break;

        }
        // delete e intent
        intent = null;
    }

}
