package com.limox.jesus.tema1;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

public class Countdown_Activity extends AppCompatActivity {
    int _counterCoffees = 0;
    int _countdown = 0;
    int _countdownStarter = 300;
    boolean _counterStarted = false;
    MyCountdown myCountdown;
    Button btnCancel;
    Button btnStart;
    Button btnPlus;
    Button btnMinus;
    ImageButton ibtnRestartCooffe;
    TextView txvNumCafes;
    TextView txvCountdown;
    Switch swUpDown;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countdown);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setEnabled(false);
        btnStart = (Button) findViewById(R.id.btnStart);
        btnPlus = (Button) findViewById(R.id.btnPlus);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        ibtnRestartCooffe = (ImageButton) findViewById(R.id.ibtnRestartCoffees);
        txvNumCafes = (TextView) findViewById(R.id.txvCoffees_Num);
        txvCountdown = (TextView) findViewById(R.id.txvCountdown);
        swUpDown = (Switch) findViewById(R.id.switch_UpDown);

        mp = MediaPlayer.create(this, R.raw.alarma);
        refreshAll();
    }


    /**
     * Classify the event onClick of the buttons
     * @param view
     */
    public void getOnClick(View view){
        switch (view.getId()){
            case R.id.btnPlus:
                addMinute();
                break;
            case R.id.btnMinus:
                subtractMinute();
                break;
            case R.id.btnStart:
                startCountdown();
                break;
            case R.id.btnCancel:
                cancelCountdown();
                break;
            case R.id.ibtnRestartCoffees:
                restartCoffees();
                break;
        }
    }

    /**
     * add a minute to the countdown value of start
     */
    private void addMinute() {
        _countdownStarter += 60;
        refreshAll();
    }

    /**
     * subtract a minute to the countdown value of start
     */
    private void subtractMinute() {
        if (_countdownStarter >= 60)
            _countdownStarter -= 60;
        refreshAll();
    }

    /**
     * restart the value of coffees drunk
     */
    private void restartCoffees(){
        _counterCoffees = 0;
        if (btnStart.isEnabled())
            btnStart.setEnabled(true);
        refreshAll();
    }

    /**
     * Start the countdown for the next coffee
     */
    private void startCountdown(){
        btnCancel.setEnabled(true);
        btnStart.setEnabled(false);
        swUpDown.setEnabled(false);
        btnPlus.setEnabled(false);
        btnMinus.setEnabled(false);
        _counterStarted = true;
        if (swUpDown.isChecked()){
            _countdown = _countdownStarter;
        }else
            _countdown = 0;
        myCountdown = new MyCountdown(_countdownStarter * 1000, 1000);
        myCountdown.start();


        refreshAll();
    }

    /**
     * Cancel the countdown
     */
    private void cancelCountdown(){
        btnCancel.setEnabled(false);
        btnStart.setEnabled(true);
        swUpDown.setEnabled(true);
        btnPlus.setEnabled(true);
        btnMinus.setEnabled(true);
        _counterStarted = false;
        _countdown = _countdownStarter;
        if (myCountdown != null){
            myCountdown.cancel();
        }
        refreshAll();
    }

    /**
     * Refresh the widgets txvNumCafes and txvCountdown;
     */
    private void refreshAll() {
        txvNumCafes.setText(String.format("%00d",_counterCoffees));
        txvCountdown.setText(Converter_Class.secondsToMinute(_countdownStarter));
    }

    /**
     * Subtract a second of the countdown
     */
    protected void substractSecon(){
        _countdown-=1;
        txvCountdown.setText(Converter_Class.secondsToMinute(_countdown));
    }

    /**
     * Add a second of the countdown
     */
    protected void addSecon(){
        _countdown+=1;
        txvCountdown.setText(Converter_Class.secondsToMinute(_countdown));
    }

    /**
     * Add a coffee to the counter
     */
    protected void addCoffees(){
        _counterCoffees += 1;
        refreshAll();
    }

    protected class MyCountdown extends CountDownTimer {

        public MyCountdown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (swUpDown.isChecked()){
                substractSecon();
            }else
                addSecon();
        }

        @Override
        public void onFinish() {
            addCoffees();
            mp.start();
            cancelCountdown();

        }
    }
}
