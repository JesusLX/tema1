package com.limox.jesus.tema1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Web_Viewer_Activity extends AppCompatActivity {

    WebView wvExplorer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_viewer);

        this.wvExplorer = (WebView) findViewById(R.id.wvExplorer);


    }

    @Override
    protected void onStart() {
        super.onStart();
        // Build a bundle with the bundle sent
        Bundle bundle = this.getIntent().getExtras();
        // Some configuration for the web viewer
        this.wvExplorer.getSettings().setLoadsImagesAutomatically(true);
        this.wvExplorer.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        // This makes the web start into the application and not an te default web explorer
        this.wvExplorer.setWebViewClient(new WebViewClient());
        // Load the web
        this.wvExplorer.loadUrl(bundle.getString("url"));
    }
}
