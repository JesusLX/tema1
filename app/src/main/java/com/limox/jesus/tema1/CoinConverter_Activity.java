package com.limox.jesus.tema1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class CoinConverter_Activity extends AppCompatActivity implements View.OnClickListener {

    EditText edtDollars, edtEuros, edtChange;
    RadioButton rbtnDollarsToEuros, rbtnEurosToDollars;
    Button btnCalculate;
    Converter_Class miConversion;
    Toast notificaion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin_converter);
        edtDollars = (EditText) findViewById(R.id.edtDollars);
        edtEuros = (EditText) findViewById(R.id.edtEuros);
        edtChange = (EditText) findViewById(R.id.edtChange);
        rbtnDollarsToEuros = (RadioButton) findViewById(R.id.rbtnDollars);
        rbtnEurosToDollars = (RadioButton) findViewById(R.id.rbtnEuros);
        btnCalculate = (Button) findViewById(R.id.btnConvert);
        btnCalculate.setOnClickListener(this);

        miConversion = new Converter_Class();
    }

    @Override
    public void onClick(View v) {
        String changeValor;
        String valorCoin;
        try {
            // Get the text of edtChange
            changeValor = edtChange.getText().toString();
            // if the var changeValor is not empty, will make the conversion
            if (!(changeValor.isEmpty() || changeValor == ".")) {
                // Check which of the radioButtons are checked to make the correct conversion
                if (rbtnDollarsToEuros.isChecked()) {
                    // convert dollars to euros
                    valorCoin = edtDollars.getText().toString();
                    if (valorCoin.isEmpty() || valorCoin == ".")
                        valorCoin = "0";

                    edtEuros.setText(String.valueOf(miConversion.dollarsToEuros(Double.parseDouble(valorCoin), Double.parseDouble(changeValor))));
                } else {

                    valorCoin = edtEuros.getText().toString();
                    if (valorCoin.isEmpty() || valorCoin == ".")
                        valorCoin = "0";
                    edtDollars.setText(String.valueOf(miConversion.eurosToDollars(Double.parseDouble(valorCoin), Double.parseDouble(changeValor))));
                }
            }
        } catch (NumberFormatException ex) {
            notificaion = Toast.makeText(this, "Error al convertir el numero", Toast.LENGTH_LONG);
            notificaion.show();
        } catch (NullPointerException ex) {
            notificaion = Toast.makeText(this, "Error al convertir el numero", Toast.LENGTH_LONG);
            notificaion.show();
        }
    }
}

