package com.limox.jesus.tema1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Web_Searcher_Activity extends AppCompatActivity {
    private Button btnSearch;
    private EditText edtURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_searcher);
        this.btnSearch = (Button) findViewById(R.id.btnSearch);
        this.edtURL = (EditText) findViewById(R.id.edtURL);
    }

    protected void getOnClick(View view){
        // Create the bundle
        Bundle bundle = new Bundle();
        // put in the bundle the url of the website and the tag to recognize
        bundle.putString("url",edtURL.getText().toString());
        // Build the intent
        Intent intent = new Intent(Web_Searcher_Activity.this,Web_Viewer_Activity.class);
        // Insert the bundle in the intent
        intent.putExtras(bundle);
        // Start the activity
        startActivity(intent);
    }
}
