package com.limox.jesus.tema1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class CmToInches_Activity extends AppCompatActivity {

    EditText edtCentimetres;
    TextView txvInches;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cm_to_inches);
        edtCentimetres = (EditText) findViewById(R.id.edtCentimetres);
        txvInches = (TextView) findViewById(R.id.txvInches);
    }

    /**
     * Calculate the equivalent of centimetres at hinches
     */
    public void getOnClick(View view) {

        try {
            double result;
            String inches = edtCentimetres.getText().toString();
            // Use the class Converter_Class to take the result
            result = Converter_Class.cmToInches(Double.parseDouble(inches));
            // Set the text result
            txvInches.setText(Double.toString(result));
        }catch (Exception e)
        {
            txvInches.setText("0");
        }

    }
}
