package com.limox.jesus.tema1;

/**
 * Created by jesus on 25/09/16.
 */

public class Converter_Class {

    double _equivalentDtoE = 0.98;
    double _equivalentEtoD = 1.12;

    /**
     * Convert centimetres to inches in a double format
     * @param cm centimetres to convert
     * @return the equivalent of cm introduced
     */
    public static double cmToInches(double cm){
        double result = 0.0;
        double _inchesEquivalentToCm = 0.39370;

        result = cm * _inchesEquivalentToCm;
        return  result;
    }

    /**
     * Convert euros to dollars with the default change valor of the class
     * @param euros amount of euros
     * @return the equivalence at dollars
     */
    public double eurosToDollars(double euros)
    {
        double result = 0.0;

        result = euros * _equivalentEtoD;
        return  result;
    }

    /**
     * Convert euros to dollars with the default change valor of the class
     * @param dollars amount of dollars
     * @return the equivalence at euros
     */
    public double dollarsToEuros(double dollars)
    {
        double result = 0.0;

        result = dollars * _equivalentDtoE;
        return  result;
    }

    /**
     * Convert euros to dollars with the change valor introduced
     * @param euros euros who want to convert to dollars
     * @param equivalentEtoD the change valor between euros and dollars
     * @return the equivalence at dollars
     */
    public static double eurosToDollars(double euros, double equivalentEtoD)
    {
        double result = 0.0;

        result = euros * equivalentEtoD;
        return  result;
    }

    /**
     * Convert dollars to euros with the change valor introduced
     * @param dollars dollars who want to convert to euros
     * @param equivalentEtoD the change valor between euros and dollars
     * @return the equivalence at euros
     */
    public static double dollarsToEuros(double dollars, double equivalentEtoD)
    {
        double result = 0.0;

        result = dollars / equivalentEtoD;
        return  result;
    }

    /**
     * Convert seconds to minute:seconds with the format 00:00
     * @param seconds seconds who want to change to seconds
     * @return minute:seconds
     */
    public static String secondsToMinute(int seconds){
        int minutes = 0;
        int restSeconds = 0;
        int oneMinuteValor = 60;
        try {
            minutes = seconds / oneMinuteValor;
        }catch (ArithmeticException e){
            minutes = 0;
        }
        restSeconds = seconds % oneMinuteValor;
        return String.format("%02d", minutes) +":"+ String.format("%02d", restSeconds);

    }
}
