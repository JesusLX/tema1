package com.limox.jesus.tema1;

import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class Hangman_Activity extends AppCompatActivity implements View.OnClickListener {
    int vidas = 6;
    // Array with the words who is going to appear in to te game.
    String[] words = {"CERDO", "DINOSAURIO", "ODISEA", "BONITO", "ILIADA", "AVENTURA", "SERIE", "DIBUJO", "HEROE", "ESPONJA", "DOMINGO", "FLAMENCO", "CANGREJO", "COSMOS"};
    // Array of images of the lives of de game
    int[] images = {R.drawable.h6, R.drawable.h5, R.drawable.h4, R.drawable.h3, R.drawable.h2, R.drawable.h1, R.drawable.h0};

    ImageView ivHangman;
    TextView txvWord;
    TextView txvErrors;
    EditText edtTry;
    String wordPicked;
    Button btnTry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hangman);
        ivHangman = (ImageView) findViewById(R.id.imageView);
        txvWord = (TextView) findViewById(R.id.txvWord);
        txvWord.setText(generateLines(pickUpWord(words)));
        txvErrors = (TextView) findViewById(R.id.txvErrors_view);
        edtTry = (EditText) findViewById(R.id.edtTry);
        btnTry = (Button) findViewById(R.id.btnTry);
        btnTry.setOnClickListener(this);
    }

    /**
     * Pick up one of the words of the array of words and use it for the game
     *
     * @param words array of words for play
     * @return the chousen one word
     */
    private String pickUpWord(String[] words) {
        Random rnd = new Random();
        wordPicked = words[rnd.nextInt(words.length)];
        return wordPicked;
    }

    /**
     * Change the chars of a word for "_"
     *
     * @param word
     * @return return a word of "_" with chars equals
     */
    private String generateLines(String word) {
        String wordAtLines = new String();
        for (int i = 0; i < word.length(); i++) {
            wordAtLines += "_";
        }
        return wordAtLines;
    }

    /**
     * Replace the "_" of the word of the game for the char who correspond if it exists
     *
     * @param wordGame    word who is in game (who have the "_")
     * @param wordAnswer  word who is the answer of the game
     * @param charToPutIn char who you want check
     * @return the wordGame with the charToPutIn introduced with as many
     * characters as there are letters the word
     */
    private String replaceChar(String wordGame, String wordAnswer, char charToPutIn) {
        char[] tmpWord = wordAnswer.toCharArray();
        char[] tmpWordGame = wordGame.toCharArray();
        //scan the wordAnswer and if find the char who is searching and if find it, change the equivalent
        // character of the wordGame
        for (int i = 0; i < tmpWord.length; i++) {
            if (tmpWord[i] == charToPutIn) {
                tmpWordGame[i] = charToPutIn;
            }
        }
        return String.valueOf(tmpWordGame);
    }

    /**
     * Check if the wordGame is completed or you are loose
     *
     * @param wordGame   word played
     * @param wordAnswer word answer
     * @return return if the wordGame is complete
     */
    private boolean haveAWinner(String wordGame, String wordAnswer) {
        boolean winner = wordAnswer.equals(wordGame);
        if (youLoose())
            btnTry.setEnabled(false);
        else if (wordAnswer.equals(wordGame))
            btnTry.setEnabled(false);
        return winner;
    }

    /**
     * Check if you have lives
     *
     * @return if you have lives
     */
    private boolean youLoose() {
        if (vidas == 0) {
            return true;
        }
        return false;
    }

    /**
     * Subtract one life and change the image of the hangman
     */
    private void subtractLives() {
        if (vidas > 0) {
            vidas -= 1;
        }
        ivHangman.setImageDrawable(getResources().getDrawable(images[vidas]));
    }

    /**
     * try to put the char of edtTry int the wordGame
     *
     * @return if the char is in the wordGame
     */
    private boolean tryChar() {
        try {
            txvWord.setText(replaceChar(txvWord.getText().toString(), wordPicked, ((edtTry.getText().toString().toUpperCase().charAt(0)))));
            return wordPicked.contains(edtTry.getText().toString().toUpperCase());
        } catch (Exception e) {
            return true;
        }

    }

    @Override
    /**
     * Try a char and if is wrong, it subtract a live
     */
    public void onClick(View v) {

        if (!tryChar()) {
            subtractLives();
            // Check if the txvErrors who contains the wrong chars used (to help the player)
            //  still contain or not the last wrong char introduced, and if is not, write it on the
            //  txvErrors.
            if (!(txvErrors.getText().toString().contains(edtTry.getText().toString().toUpperCase()))) {
                txvErrors.setText(txvErrors.getText().toString() + edtTry.getText().toString().toUpperCase());
            }


        }
        haveAWinner(txvWord.getText().toString(), wordPicked);
        // Clear the char on edtTry to make easiest play for the user
        edtTry.setText("");
    }
}
