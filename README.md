Tema 1
----------
 Ejercicios de introducción
=====================

En esta primera aplicación consta de una *Activity* con *cinco botones* que cada uno lleva a otra *Activity* cada una diferente de la anterior.
Las siguientes *Activities* contienen:
1. **Convertidor de divisas.** 
2. **Calculadora de centímetros a pulgadas.**
3. **Explorador web.**
4. **Contador de cafés con alarma.**
5. **Juego del Ahorcado**

----------
Activity Principal
------------------------

![// Imagen 1](https://bitbucket.org/repo/9rA5bL/images/975043374-Screenshot_20161009-133718.png)

Aquí se presenta la primera Activity con los cinco botones nombrado anteriormente, los botones tienen los colores predefinidos por mi para aspecto único de la aplicación.


1. Convertidor de divisas
-------------
 ----------------------------------
![ // Imagen 2](https://bitbucket.org/repo/9rA5bL/images/1740161257-Screenshot_20161009-133735.png)

Aquí se muestra el *conversor* de divisas entre Euros y dolares, con un **switch** que permite decidir *de qué moneda a qué moneda* hacer el cambio de valor cuando se pulse el botón, también  se compone de un ultimo campo en el que se puede escribir el **valor de cambio** entre dolares y euros.

2. Calculador Cm a Pulgadas
----------------------------------------
----------------------
![// Imagen 3](https://bitbucket.org/repo/9rA5bL/images/813522801-Screenshot_20161009-133803.png)

Es una simple calculadora en la que introduces la cantidad en centímetros y la convierte a pulgadas al pulsar el botón *calcular*

3. Explorador web
----------------------------------------
---------------------------
Esta parte de la aplicación se compone de dos Activities:

![// Imagen cuatro](https://bitbucket.org/repo/9rA5bL/images/1576569502-Screenshot_20161009-133900.png)

 la primera es simplemente un cuadro de texto editable en el que se introduce una dirección web y al pulsar el botón se manda a la segunda Activity.
![
// Imagen cinco](https://bitbucket.org/repo/9rA5bL/images/2913969024-Screenshot_20161009-133907.png)

Esta se compone de un objeto de visualización web que muestra dentro de la aplicación la página web escrita en la vista anterior.

4. Contador de cafés 
-------------------------------
---------------------
![// Imagen 6](https://bitbucket.org/repo/9rA5bL/images/3024554686-Screenshot_20161009-134442.png)

Esta Activity es algo mas compleja, de arriba abajo se compone de un botón para reiniciar el *botón* de cafés, a su lado la cantidad de cafés tomados; bajo este un *switch* que te permite decidir si la cuenta atrás del tiempo previamente estimado va de *0 a X o* de *X a 0.*
Bajo esto se encuentra el contador de tiempo, con botones a ambos lados para añadir o sustraer minutos a la cuenta de tiempo hasta el próximo café. En la parte mas baja de la aplicación se encuentran dos botones: uno para **comenzar** la *cuenta atrás* y otro para **cancelarla**. 

5. Juego El ahorcado   
-------------------------------
------------
![// Imagen 7](https://bitbucket.org/repo/9rA5bL/images/702044007-Screenshot_20161009-134454.png)

El juego se basa en acertar una palabra diciendo letras al azar que la vayan completando, si se dice alguna errónea se va dibujando el cuerpo de un hombre ahorcado, si se completa el cuerpo, el jugador pierde.

![// Imagen 8](https://bitbucket.org/repo/9rA5bL/images/2891924108-Screenshot_20161009-134504.png)

Las letras erróneas se muestran encima del dibujo para ayudar al jugador a no cometer el mismo error dos vences, pero por si alguna razón lo volviese a hacer se le tomará como otro error y se dibujará otra parte del ahorcado.

![// Imagen 9](https://bitbucket.org/repo/9rA5bL/images/3604274871-Screenshot_20161009-134512.png)

Las letras acertadas se irán colocando una vez dichas en su posición original en la palabra que se está intentado acertar, si la palabra contiene esa letra mas de una vez las escribirá tantas veces como haga falta.

![// Imagen 10](https://bitbucket.org/repo/9rA5bL/images/2914815516-Screenshot_20161009-134537.png)

Si la palabra o el muñeco es completada terminará el juego, con la diferencia que si completas la palabra ganas y si completas al muñeco pierdes.